require 	'rails_helper'

describe EventJobJobAssociation do
  before do
    @event_job_job_association= EventJobJobAssociation.new(event_id:"2",predecessor_id:"3", successor_id:"4")
  end

  subject { @event_job_job_association }

  it {should respond_to(:event_id) }
  it {should respond_to(:predecessor_id) }
  it {should respond_to(:successor_id) }

  it {should be_valid }

  describe "when event_id is not present" do
    before {@event_job_job_association.event_id = "" }
    it {should_not be_valid }
  end

  describe "when predecessor_id is not present" do
    before {@event_job_job_association.predecessor_id = "" }
    it {should_not be_valid }
  end

  describe "when successor_id is not present" do
    before {@event_job_job_association.successor_id = "" }
    it {should_not be_valid }
  end
end
