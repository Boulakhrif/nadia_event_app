class EventJobPeriodAssociation < ActiveRecord::Base
  belongs_to :event
  belongs_to :job
  belongs_to :period
  validates :event_id, presence: true
  validates :job_id, presence: true
end
