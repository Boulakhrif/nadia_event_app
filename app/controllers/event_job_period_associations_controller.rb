class EventJobPeriodAssociationsController < ApplicationController
  before_action :set_event_job_period_association, only: [:show, :edit, :update, :destroy]

  # GET /event_job_period_associations
  # GET /event_job_period_associations.json
  def index
    @event_job_period_associations = EventJobPeriodAssociation.all
  end

  # GET /event_job_period_associations/1
  # GET /event_job_period_associations/1.json
  def show
  end

  # GET /event_job_period_associations/new
  def new
    @event_job_period_association = EventJobPeriodAssociation.new
  end

  # GET /event_job_period_associations/1/edit
  def edit
  end

  # POST /event_job_period_associations
  # POST /event_job_period_associations.json
  def create
    @event_job_period_association = EventJobPeriodAssociation.new(event_job_period_association_params)

    respond_to do |format|
      if @event_job_period_association.save
        format.html { redirect_to @event_job_period_association, notice: 'Der Event-Vorgang wurde erfolgreich angelegt.' }
        format.json { render :show, status: :created, location: @event_job_period_association }
      else
        format.html { render :new }
        format.json { render json: @event_job_period_association.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /event_job_period_associations/1
  # PATCH/PUT /event_job_period_associations/1.json
  def update
    respond_to do |format|
      if @event_job_period_association.update(event_job_period_association_params)
        format.html { redirect_to @event_job_period_association, notice: 'Der Event-Vorgang wurde erfolgreich aktualisiert.' }
        format.json { render :show, status: :ok, location: @event_job_period_association }
      else
        format.html { render :edit }
        format.json { render json: @event_job_period_association.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /event_job_period_associations/1
  # DELETE /event_job_period_associations/1.json
  def destroy
    @event_job_period_association.destroy
    respond_to do |format|
      format.html { redirect_to event_job_period_associations_url,
                                notice: 'Der Event-Vorgang wurde entfernt.' }
      format.json { head :no_content }
    end
  end

  def optimize_and_read

    if File.exist?("Multi_Projekt_Daten.inc")
      File.delete("Multi_Projekt_Daten.inc")
    end
    f=File.new("Multi_Projekt_Daten.inc", "w")

    # Events
    printf(f, "set p / \n")
    @events = Event.all
    @events.each { |event| printf(f, "'"+event.name + "'\n") }
    printf(f, "/" + "\n\n")

    # Vorgänge
    printf(f, "set i / \n")
    @jobs = Job.all
    @jobs.each { |job| printf(f, "'"+job.name + "'\n") }
    printf(f, "/" + "\n\n")

    # Perioden
    printf(f, "set t / \n")
    @periods = Period.all
    @periods.each { |per| printf(f, "'"+ per.id.to_s + "'\n") }
    printf(f, "/;\n\n")

    printf(f, "set pi / \n")
    @event_job_period_associations = EventJobPeriodAssociation.all
    @event_job_period_associations.each { |eve_job_per| printf(f, "pi" +
        eve_job_per.id.to_s + "\n") }
    printf(f, "/;" + "\n\n")

    printf(f, "PIpi(pi,p,i)=no;" + "\n\n")
    @event_job_period_associations.each { |eve_job_per|
      printf(f, "PIpi('pi"+ eve_job_per.id.to_s + "',
      '" + eve_job_per.event.name + "',
      '" + eve_job_per.job.name + "')=yes ; \n")
    }
    printf(f, "\n\n")

    printf(f, "\n")
    @events.each { |eve|
      printf(f, "Deadline('" + eve.name + "')="+ (eve.deadline-
          eve.created_at.to_date).to_s + ";\n")
    }
    printf(f, "\n")

    printf(f, "PN(p,i)=no;" + "\n\n")

    @jobs = Job.all
    @jobs.each { |job|
      printf(f, "PN('" + job.event.name+"','" + job.name+"')=yes;\n") }

    # Vorgänger-Nachfolger-Beziehung
    printf(f, "VN(p,h,i)=no;" + "\n\n")
    @event_job_job_association=EventJobJobAssociation.all
    @event_job_job_association.each{|eve_jojo|
      printf(f, "VN('" + eve_jojo.event.name+"',
      '" + eve_jojo.predecessor.name+"',
      '" + eve_jojo.successor.name+"')=yes;\n" )}

    # Bearbeitungsdauer und Kapazitätsbedarf
    @jobs=Job.all
    @jobs.each{|job|
      printf(f, "d('" + job.name+"')= "+job.processing_time.to_s + ";\n")
      printf(f, "k('" + job.name+"')= "+job.ressource_demand.to_s + ";\n")
      printf(f, "\n")
    }

    # Verfügbare Kapazität und Kosten zusätzlicher Kapazität
    printf(f, "KP=6;\n")
    printf(f, "oc=160;\n")

    f.close

    if File.exist?("TCPSP_solution.txt")
      File.delete("TCPSP_solution.txt")
    end

    system GamsPath.find(1).gams_path_url + " Multi_Projekt_TCPSP"

    flash.now[:started] = "Die Rechnung wurde gestartet!"

    # Zielfunktionswert einlesen
    if File.exist?("TCPSP_Zfkt.txt")
      fi=File.open("TCPSP_Zfkt.txt", "r")
      line=fi.readline
      fi.close
      sa=line.split(" ")
      @objective_function_value=sa[1]
    else
      @objective_function_value=nil

      flash.now[:not_available] = "Der Zielfunktionswert wurde noch nicht
      berechnet!"

    end

    if (File.exists?("TCPSP_solution_PI.txt") and
        File.exists?("TCPSP_solution_add_cappa.txt"))

      # Endzeitpunkte der Vorgänge einlesen
      fi=File.open("TCPSP_solution_PI.txt", "r")
      fi.each { |line|
        sa=line.split(";")
        sa0=sa[0].delete "pi "
        sa3=sa[3]
        sa4=sa[4].delete " \n"
        @event_job_period_associations=
            EventJobPeriodAssociation.find_by_id(sa0)
        @event_job_period_associations.period_id=sa3
        @event_job_period_associations.job_end=sa4
        @event_job_period_associations.save
      }
      fi.close

      # Zusätzliche Kapazität pro Periode einlesen
      fi=File.open("TCPSP_solution_add_cappa.txt", "r")
      fi.each { |line|
        sa=line.split(";")
        sa0=sa[0].delete " "
        sa1=sa[1].delete " \n"
        @period=Period.find_by_id(sa0)
        @period.add_cappa=sa1
        @period.save
      }
      fi.close

    else
      flash.now[:not_available] = "Die Lösung wurde noch nicht berechnet!"
    end
    @event_job_period_associations = EventJobPeriodAssociation.all
    render :template => "static_pages/tcpsp"

  end


  def delete_old_plan

    if File.exist?("TCPSP_solution_add_cappa.txt")
      File.delete("TCPSP_solution_add_cappa.txt")
    end

    if File.exist?("TCPSP_solution_PI.txt")
      File.delete("TCPSP_solution_PI.txt")
    end

    if File.exist?("TCPSP_solution_PN.txt")
      File.delete("TCPSP_solution_PN.txt")
    end

    if File.exist?("TCPSP_Zfkt.txt")
      File.delete("TCPSP_Zfkt.txt")
    end

    @event_job_period_associations = EventJobPeriodAssociation.all
    @event_job_period_associations.each { |eve_job_per|
      eve_job_per.period_id=nil
      eve_job_per.job_end=nil
      eve_job_per.save
    }

    @periods=Period.all
    @periods.each { |per|
      per.add_cappa=nil
      per.save
    }

    @objective_function_value=nil

    render :template => "periods/index"
  end



  private
  # Use callbacks to share common setup or constraints between actions.
  def set_event_job_period_association
    @event_job_period_association = EventJobPeriodAssociation.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def event_job_period_association_params
    params.require(:event_job_period_association).permit(:event_id, :job_id, :period_id, :job_end)
  end
end
