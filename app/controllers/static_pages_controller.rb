class StaticPagesController < ApplicationController
  def home
  end

  def impression
    if logged_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
    @microposts = Micropost.all
  end

  def offer
  end

  def about
  end

  def contact
  end

  def impressum
  end
end
